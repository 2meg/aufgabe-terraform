terraform {
  required_version = ">=0.12"
  backend "s3" {
    bucket = "aufgabe-tf-s3"
    key = "aufgabe/state.tfstate"
    region = "eu-central-1"
  }
}

# provider "mongodbatlas" {
#   public_key = var.mongodbatlas_public_key
#   private_key  = var.mongodbatlas_private_key
# }

provider "aws" {
    region = var.region
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "aufgabe_vpc"
  cidr = "10.0.0.0/16"
  create_igw = true


  azs             = ["eu-central-1a"] #, "eu-central-1b"
  public_subnets  = ["10.0.1.0/24"] #, "10.0.2.0/24"

  enable_nat_gateway = false
  enable_vpn_gateway = false
  tags = {
      Name = "${var.env_prefix}_vpc"
  }
}


module "webserver_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "webserver_sg"
  description = "Security group for my server"
  vpc_id = module.vpc.vpc_id


  ingress_cidr_blocks      = ["0.0.0.0/0"] # try out all
  ingress_rules            = ["ssh-tcp", "http-80-tcp"]
  egress_with_cidr_blocks = [
    {
      from_port = 0 
      to_port = 0
      protocol = "-1"
      cidr_blocks = "0.0.0.0/0"
    }
  ]
}

resource "aws_key_pair" "key_aufgabe" {
    key_name = "key_aufgabe"
    public_key = file(var.public_key_location)
}

resource "aws_route_table" "aufgabe_rt" {
    vpc_id = module.vpc.vpc_id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = module.vpc.igw_id
    }
    tags = {
        Name: "${var.env_prefix}_rt"
    }
}

resource "aws_route53_record" "prod" {
  zone_id = var.prod_hosted_zone_id
  name    = "www.aufgabe.myroslav-symchych.space"
  type    = "A"
  ttl     = "300"
  records = [module.webserver_prod.public_ip]
}

resource "aws_route53_record" "qa" {
  zone_id = var.qa_hosted_zone_id
  name    = "qa.aufgabe.myroslav-symchych.space"
  type    = "A"
  ttl     = "300"
  records = [module.webserver_stage.public_ip]
}



# resource "aws_internet_gateway" "aufgabe_gateway" {
#     vpc_id = module.vpc.vpc_id
#     tags = {
#         Name: "${var.env_prefix}_gw"
#     }
# }

# resource "aws_route_table_association" "subnet_1_a_association" {
#     subnet_id = module.vpc.public_subnets[0]
#     route_table_id = aws_route_table.aufgabe_rt.id
# }

/*resource "aws_db_subnet_group" "db_subnet_group" {
    name = "aufgabe_db_subnet_groups"
    subnet_ids = [module.vpc.private_subnets[0]]

    tags = {
        Name = "My DB subnet group"
        }
}*/

