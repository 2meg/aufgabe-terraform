#!/bin/bash
sudo yum update -y
sudo amazon-linux-extras install ansible2 -y
mkdir ~/.aws
touch ~/.aws/credentials
echo "aws_access_key_id =  ${var.aws_access_key}" > ~/.aws/credentials
echo "aws_secret_access_key = ${var.aws_secret_key}" >> ~/.aws/credentials
