output webserver_prod_ip {
    value = module.webserver_prod.public_ip
}

output webserver_stage_ip {
    value = module.webserver_stage.public_ip
}

output webserver_ansible_ip {
    value = module.ansible_server.public_ip
}