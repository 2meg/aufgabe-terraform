module "webserver_prod" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "${var.prod_env_prefix}_webserver"

  ami                    = data.aws_ami.latest_ami_image.id
  instance_type          = var.instance_type
  key_name               = aws_key_pair.key_aufgabe.key_name
  monitoring             = false
  vpc_security_group_ids = [module.webserver_sg.security_group_id]
  subnet_id              = module.vpc.public_subnets[0]

  tags = {
    Terraform   = "true"
    Environment = "dev"
    name = "Aufgabe_Server"
  }
}

module "webserver_stage" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "${var.stage_env_prefix}_webserver"

  ami                    = data.aws_ami.latest_ami_image.id
  instance_type          = var.instance_type
  key_name               = aws_key_pair.key_aufgabe.key_name
  monitoring             = false
  vpc_security_group_ids = [module.webserver_sg.security_group_id]
  subnet_id              = module.vpc.public_subnets[0]

  tags = {
    Terraform   = "true"
    Environment = "dev"
    name = "Aufgabe_Server"
  }
}

module "ansible_server" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "ansible_server"

  ami                    = data.aws_ami.latest_ami_image.id
  instance_type          = var.instance_type
  key_name               = aws_key_pair.key_aufgabe.key_name
  monitoring             = false
  vpc_security_group_ids = [module.webserver_sg.security_group_id]
  subnet_id              = module.vpc.public_subnets[0]
  user_data              = <<EOF
    #!/bin/bash
    sudo yum update -y
    sudo amazon-linux-extras install ansible2 -y
    mkdir /home/ec2-user/.aws
    touch /home/ec2-user/.aws/credentials
    echo "aws_access_key_id = ${var.aws_access_key}" > /home/ec2-user/.aws/credentials
    echo "aws_secret_access_key = ${var.aws_secret_key}" >> /home/ec2-user/.aws/credentials
  EOF

  tags = {
    Terraform   = "true"
    Environment = "dev"
    name = "Aufgabe_Server"
  }
}