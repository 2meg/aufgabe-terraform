

variable "region" {}

variable "aws_secret_key" { 
    sensitive = true
    type = string
}

variable "aws_access_key" { 
    sensitive = true
    type = string
}


variable "av_zone" {}


variable env_prefix {}

variable "egress_cidr" {}

variable "ingress_cidr_ssh" {}
variable "ingress_cidr_http" {}

# AMI fetch

variable "ami_owners" {}

variable "ami_filter_name" {}

variable "ami_filter_value" {}

variable "ami_owners_nexus" {}

variable "ami_filter_name_nexus" {}

variable "ami_filter_value_nexus" {}

# EC2 Instance

variable "instance_type" {}

variable "prod_env_prefix" {}

variable "stage_env_prefix" {}


# key pair

variable "public_key_location" {}

variable "prod_hosted_zone_id" {}

variable "qa_hosted_zone_id" {}