data "aws_ami" "latest_ami_image" {
    most_recent = true
    owners = var.ami_owners
    filter {
        name = var.ami_filter_name
        values = var.ami_filter_value
    }
}

# data "aws_ami" "latest_ami_image_for_nexus" {
#     most_recent = true
#     owners = var.ami_owners_nexus
#     filter {
#         name = var.ami_filter_name_nexus
#         values = var.ami_filter_value_nexus
#     }
# }